<?php
require 'vendor/autoload.php';

$client = new Predis\Client();
$client->set('foo', 'bar');

$character = null;

if ($_SERVER['REQUEST_METHOD'] === "POST" && $_POST["save"] === "save" ) {
    $character = $_POST;
    $character_json = json_encode($character);
    $client->set($_POST['character-select'], $character_json);
    $character = load($client, $_POST["character-select"]);
}

if ($_SERVER['REQUEST_METHOD'] === "POST" && $_POST["load"] === "load" ) {
    $character = load($client, $_POST["character-select"]);
}

function load($client, $chara){
    $value = $client->get($chara);
    if(null !==  value){
        return json_decode($value);
    }
    return null;
}

?>

<!doctype html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Trudvang - Feuille de Personnage</title>
		<link rel="stylesheet" type="text/css" href="trudvang_charactersheet.css">
        <script type="text/javascript" src="tabs.js"></script>
	</head>
	<body>
        <form id="character-sheet" method="POST">
            <!-- Header -->
            <div id="character-header">
                <fieldset id="bio1" form="character-sheet">
                    <label for="name"><input type="text" name="name" value='<?= $character === null ? "" : $character->name; ?>'> Nom</label>
                    <label for="player"><input type="text" name="player" value='<?= $character === null ? "" : $character->player; ?>'> Joueur</label>
                    <label for="race"><input type="text" name="race" value='<?= $character === null ? "" : $character->race; ?>'> Race</label>
                    <label for="archetype"><input type="text" name="archetype" value='<?= $character === null ? "" : $character->archetype; ?>'> Achétype</label>
                </fieldset>
                <fieldset id="saveload" form="character-sheet">
                    <img src="img/logo.png" id="logo">
                    <fieldset>
                        <select id="character-select" name="character-select">
                            <option value="">--Selectionner un joueur--</option>
                            <option value="Florian">Florian</option>
                            <option value="Lucas">Lucas</option>
                            <option value="Victor">Victor</option>
                        </select>
                        <input type="submit" value="load" name="load">
                        <input type="submit" value="save" name="save">
                    </fieldset>
                </fieldset>
            </div>
            <!-- Tab links -->
            <div class="tab">
                <button type="button" class="tablinks" onclick="openTab(event, 'character-bio')">Bio</button>
                <button type="button" class="tablinks" onclick="openTab(event, 'character-stats')">Stats</button>
                <button type="button" class="tablinks" onclick="openTab(event, 'character-inventory')">Inventaire</button>
                <button type="button" class="tablinks" onclick="openTab(event, 'character-state')">Combat</button>
                <button type="button" class="tablinks" onclick="openTab(event, 'character-spells')">Magie</button>
            </div>
            <!-- Tab content -->
            <div id="character-bio" class="tabcontent">
                <fieldset id="bio2" form="character-sheet">
                    <label for="gender"><input type="text" name="gender" value='<?= $character === null ? "" : $character->gender; ?>'> Sexe</label>
                    <label for="age"><input type="number" name="age" value='<?= $character === null ? "" : $character->age; ?>'> Age</label>
                    <label for="height"><input type="number" name="height" value='<?= $character === null ? "" : $character->height; ?>'> Taille</label>
                    <label for="weight"><input type="number" name="weight" value='<?= $character === null ? "" : $character->weight; ?>'> Poids</label>
                    <label for="weaponhand"><input type="text" name="weaponhand" value='<?= $character === null ? "" : $character->weaponhand; ?>'> Main d'arme</label>
                    <label for="religion"><input type="text" name="religion" value='<?= $character === null ? "" : $character->religion; ?>'> Religion</label>
                    <label for="culture"><input type="text" name="culture" value='<?= $character === null ? "" : $character->culture; ?>'> Culture</label>
                    <label for="langue"><input type="text" name="langue" value='<?= $character === null ? "" : $character->langue; ?>'> Langue maternelle</label>
                </fieldset>
                <fieldset id="pa-points">
                    <label for="advpts"><input type="number" name="advpts" value='<?= $character === null ? "" : $character->advpts; ?>'> PA dispo</label>
                    <label for="usedadvpts"><input type="number" name="usedadvpts" value='<?= $character === null ? "" : $character->usedadvpts; ?>'> PA utilisés</label>
                </fieldset>
            </div>
            <div id="character-stats" class="tabcontent">
                <fieldset id="characs-form" form="character-sheet">
                    <label for="cha"><input type="number" name="cha" value='<?= $character === null ? "" : $character->cha; ?>'> CHA</label>
                    <label for="con"><input type="number" name="con" value='<?= $character === null ? "" : $character->con; ?>'> CON</label>
                    <label for="dex"><input type="number" name="dex" value='<?= $character === null ? "" : $character->dex; ?>'> DEX</label>
                    <label for="for"><input type="number" name="for" value='<?= $character === null ? "" : $character->for; ?>'> FOR</label>
                    <label for="int"><input type="number" name="int" value='<?= $character === null ? "" : $character->int; ?>'> INT</label>
                    <label for="per"><input type="number" name="per" value='<?= $character === null ? "" : $character->per; ?>'> PER</label>
                    <label for="psy"><input type="number" name="psy" value='<?= $character === null ? "" : $character->psy; ?>'> PSY</label>
                    <label for="raud"><input type="number" name="raud" value='<?= $character === null ? "" : $character->raud; ?>'> RAUD</label>
                </fieldset>
                <fieldset id="competences">
                    <fieldset id="agility-form" class="competence-forms" form="character-sheet">
                        <label for="agilite" class="competence">Agilité</label><p></p><input name="agilite" type="text" value='<?= $character === null ? "" : $character->agilite; ?>'>
                            <label for="cc_niv" class="d-stat">Contrôle Corporel (D)</label><input name="cc_niv" type="text" value='<?= $character === null ? "" : $character->cc_niv; ?>'><input name="cc_stat" type="text" value='<?= $character === null ? "" : $character->cc_stat; ?>'>
                                <label for="ambi_niv" class="stat">Ambidextrie</label><input name="ambi_niv" type="text" value='<?= $character === null ? "" : $character->ambi_niv; ?>'><input name="ambi_stat" type="text" value='<?= $character === null ? "" : $character->ambi_stat; ?>'>
                                <label for="jong_niv" class="stat">Jonglerie</label><input name="jong_niv" type="text" value='<?= $character === null ? "" : $character->jong_niv; ?>'><input name="jong_stat" type="text" value='<?= $character === null ? "" : $character->jong_stat; ?>'>
                                <label for="nat_niv" class="stat">Natation</label><input name="nat_niv" type="text" value='<?= $character === null ? "" : $character->nat_niv; ?>'><input name="nat_stat" type="text" value='<?= $character === null ? "" : $character->nat_stat; ?>'>
                                <label for="see_niv" class="stat">Saut, escalade et équilibre</label><input name="see_niv" type="text" value='<?= $character === null ? "" : $character->see_niv; ?>'><input name="see_stat" type="text" value='<?= $character === null ? "" : $character->see_stat; ?>'>
                            <label for="mait_equ_niv" class="d-stat">Maîtrise Équestre (D)</label><input name="mait_equ_niv" type="text" value='<?= $character === null ? "" : $character->mait_equ_niv; ?>'><input name="mait_equ_stat" type="text" value='<?= $character === null ? "" : $character->mait_equ_stat; ?>'>
                                <label for="cdc_niv" class="stat">Conduite de chariot</label><input name="cdc_niv" type="text" value='<?= $character === null ? "" : $character->cdc_niv; ?>'><input name="cdc_stat" type="text" value='<?= $character === null ? "" : $character->cdc_stat; ?>'>
                                <label for="equi_niv" class="stat">Équitation</label><input name="equi_niv" type="text" value='<?= $character === null ? "" : $character->equi_niv; ?>'><input name="equi_stat" type="text" value='<?= $character === null ? "" : $character->equi_stat; ?>'>
                            <label for="mdc_niv" class="d-stat">Manoeuvre de Combat (D)</label><input name="mdc_niv" type="text" value='<?= $character === null ? "" : $character->mdc_niv; ?>'><input name="mdc_stat" type="text" value='<?= $character === null ? "" : $character->mdc_stat; ?>'>
                                <label for="cuir_niv" class="stat">Cuirassé</label><input name="cuir_niv" type="text" value='<?= $character === null ? "" : $character->cuir_niv; ?>'><input name="cuir_stat" type="text" value='<?= $character === null ? "" : $character->cuir_stat; ?>'>
                                <label for="esqu_niv" class="stat">Esquive</label><input name="esqu_niv" type="text" value='<?= $character === null ? "" : $character->esqu_niv; ?>'><input name="esqu_stat" type="text" value='<?= $character === null ? "" : $character->esqu_stat; ?>'>
                                <label for="mouvdc_niv" class="stat">Mouvement de Combat</label><input name="mouvdc_niv" type="text" value='<?= $character === null ? "" : $character->mouvdc_niv; ?>'><input name="mouvdc_stat" type="text" value='<?= $character === null ? "" : $character->mouvdc_stat; ?>'>
                    </fieldset>
                    <fieldset id="artdesombres-form" class="competence-forms" form="character-sheet">
                        <label for="artsdesombres" class="competence">Arts des Ombres</label><p></p><input name="artsdesombres" type="text" value='<?= $character === null ? "" : $character->artsdesombres; ?>'>
                            <label for="disc_niv" class="d-stat">Discrétion (D)</label><input name="disc_niv" type="text" value='<?= $character === null ? "" : $character->disc_niv; ?>'><input name="disc_stat" type="text" value='<?= $character === null ? "" : $character->disc_stat; ?>'>
                                <label for="attfurt_niv" class="stat">Attaque furtive</label><input name="attfurt_niv" type="text" value='<?= $character === null ? "" : $character->attfurt_niv; ?>'><input name="attfurt_stat" type="text" value='<?= $character === null ? "" : $character->attfurt_stat; ?>'>
                                <label for="ced_niv" class="stat">Camouflage et Dissimulation</label><input name="ced_niv" type="text" value='<?= $character === null ? "" : $character->ced_niv; ?>'><input name="ced_stat" type="text" value='<?= $character === null ? "" : $character->ced_stat; ?>'>
                                <label for="mdlo_niv" class="stat">Marcher dans les ombres</label><input name="mdlo_niv" type="text" value='<?= $character === null ? "" : $character->mdlo_niv; ?>'><input name="mdlo_stat" type="text" value='<?= $character === null ? "" : $character->mdlo_stat; ?>'>
                                <label for="ter_niv" class="stat">Trouver et remarquer</label><input name="ter_niv" type="text" value='<?= $character === null ? "" : $character->ter_niv; ?>'><input name="ter_stat" type="text" value='<?= $character === null ? "" : $character->ter_stat; ?>'>
                            <label for="larcin_niv" class="d-stat">Larcin (D)</label><input name="larcin_niv" type="text" value='<?= $character === null ? "" : $character->larcin_niv; ?>'><input name="larcin_stat" type="text" value='<?= $character === null ? "" : $character->larcin_stat; ?>'>
                                <label for="deg_niv" class="stat">Déguisement</label><input name="deg_niv" type="text" value='<?= $character === null ? "" : $character->deg_niv; ?>'><input name="deg_stat" type="text" value='<?= $character === null ? "" : $character->deg_stat; ?>'>
                                <label for="mdo_niv" class="stat">Monde des ombres</label><input name="mdo_niv" type="text" value='<?= $character === null ? "" : $character->mdo_niv; ?>'><input name="mdo_stat" type="text" value='<?= $character === null ? "" : $character->mdo_stat; ?>'>
                                <label for="sep_niv" class="stat">Serrures et pièges</label><input name="sep_niv" type="text" value='<?= $character === null ? "" : $character->sep_niv; ?>'><input name="sep_stat" type="text" value='<?= $character === null ? "" : $character->sep_stat; ?>'>
                                <label for="sdv_niv" class="stat">Signes de voleur</label><input name="sdv_niv" type="text" value='<?= $character === null ? "" : $character->sdv_niv; ?>'><input name="sdv_stat" type="text" value='<?= $character === null ? "" : $character->sdv_stat; ?>'>
                                <label for="voler_niv" class="stat">Voler</label><input name="voler_niv" type="text" value='<?= $character === null ? "" : $character->voler_niv; ?>'><input name="voler_stat" type="text" value='<?= $character === null ? "" : $character->voler_stat; ?>'>
                    </fieldset>
                    <fieldset id="combat-form" class="competence-forms" form="character-sheet">
                        <label for="combat" class="competence">Combat</label><p></p><input name="combat" type="text" value='<?= $character === null ? "" : $character->combat; ?>'>
                            <label for="camn_niv" class="d-stat">Combat à mains nues (D)</label><input name="camn_niv" type="text" value='<?= $character === null ? "" : $character->camn_niv; ?>'><input name="camn_stat" type="text" value='<?= $character === null ? "" : $character->camn_stat; ?>'>
                                <label for="bag_niv" class="stat">Bagarre</label><input name="bag_niv" type="text" value='<?= $character === null ? "" : $character->bag_niv; ?>'><input name="bag_stat" type="text" value='<?= $character === null ? "" : $character->bag_stat; ?>'>
                                <label for="lut_niv" class="stat">Lutte</label><input name="lut_niv" type="text" value='<?= $character === null ? "" : $character->lut_niv; ?>'><input name="lut_stat" type="text" value='<?= $character === null ? "" : $character->lut_stat; ?>'>
                            <label for="com_arm_niv" class="d-stat">Combat armé (D)</label><input name="com_arm_niv" type="text" value='<?= $character === null ? "" : $character->com_arm_niv; ?>'><input name="com_arm_stat" type="text" value='<?= $character === null ? "" : $character->com_arm_stat; ?>'>
                                <label for="arb_niv" class="stat">Arbalète</label><input name="arb_niv" type="text" value='<?= $character === null ? "" : $character->arb_niv; ?>'><input name="arb_stat" type="text" value='<?= $character === null ? "" : $character->raud; ?>'>
                                <label for="aef_niv" class="stat">Arcs et frondes</label><input name="aef_niv" type="text" value='<?= $character === null ? "" : $character->aef_niv; ?>'><input name="aef_stat" type="text" value='<?= $character === null ? "" : $character->aef_stat; ?>'>
                                <label for="aadm_niv" class="stat">Armes à deux mains</label><input name="aadm_niv" type="text" value='<?= $character === null ? "" : $character->aadm_niv; ?>'><input name="aadm_stat" type="text" value='<?= $character === null ? "" : $character->aadm_stat; ?>'>
                                <label for="adl_niv" class="stat">Armes de lancer</label><input name="adl_niv" type="text" value='<?= $character === null ? "" : $character->adl_niv; ?>'><input name="adl_stat" type="text" value='<?= $character === null ? "" : $character->adl_stat; ?>'>
                                <label for="alumd_niv" class="stat">Armes légères à une main (d)</label><input name="alumd_niv" type="text" value='<?= $character === null ? "" : $character->alumd_niv; ?>'><input name="alumd_stat" type="text" value='<?= $character === null ? "" : $character->alumd_stat; ?>'>
                                <label for="alumg_niv" class="stat">Armes légères à une main (g)</label><input name="alumg_niv" type="text" value='<?= $character === null ? "" : $character->alumg_niv; ?>'><input name="alumg_stat" type="text" value='<?= $character === null ? "" : $character->alumg_stat; ?>'>
                                <label for="pdb_niv" class="stat">Porteur de bouclier</label><input name="pdb_niv" type="text" value='<?= $character === null ? "" : $character->pdb_niv; ?>'><input name="pdb_stat" type="text" value='<?= $character === null ? "" : $character->pdb_stat; ?>'>
                            <label for="expcomb_niv" class="d-stat">Expérience de combat (D)</label><input name="expcomb_niv" type="text" value='<?= $character === null ? "" : $character->expcomb_niv; ?>'><input name="expcomb_stat" type="text" value='<?= $character === null ? "" : $character->expcomb_stat; ?>'>
                                <label for="adc_niv" class="stat">Actions de combat</label><input name="adc_niv" type="text" value='<?= $character === null ? "" : $character->adc_niv; ?>'><input name="adc_stat" type="text" value='<?= $character === null ? "" : $character->adc_stat; ?>'>
                                <label for="ca_niv" class="stat">Chargeur d'arbalète</label><input name="ca_niv" type="text" value='<?= $character === null ? "" : $character->ca_niv; ?>'><input name="ca_stat" type="text" value='<?= $character === null ? "" : $character->ca_stat; ?>'>
                                <label for="comb_niv" class="stat">Combattant</label><input name="comb_niv" type="text" value='<?= $character === null ? "" : $character->comb_niv; ?>'><input name="comb_stat" type="text" value='<?= $character === null ? "" : $character->comb_stat; ?>'>
                                <label for="portarm_niv" class="stat">Porteur d'armure</label><input name="portarm_niv" type="text" value='<?= $character === null ? "" : $character->portarm_niv; ?>'><input name="portarm_stat" type="text" value='<?= $character === null ? "" : $character->portarm_stat; ?>'>
                                <label for="reaccom_niv" class="stat">Réaction de combat</label><input name="reaccom_niv" type="text" value='<?= $character === null ? "" : $character->reaccom_niv; ?>'><input name="reaccom_stat" type="text" value='<?= $character === null ? "" : $character->reaccom_stat; ?>'>
                    </fieldset>
                    <fieldset id="connaissances-form" class="competence-forms" form="character-sheet">
                        <label for="connaissances" class="competence">Connaissances</label><p></p><input name="connaissances" type="text" value='<?= $character === null ? "" : $character->connaissances; ?>'>
                            <label for="appr_niv" class="d-stat">Apprentissage (D)</label><input name="appr_niv" type="text" value='<?= $character === null ? "" : $character->appr_niv; ?>'><input name="appr_stat" type="text" value='<?= $character === null ? "" : $character->appr_stat; ?>'>
                                <label for="erud_niv" class="stat">Érudition</label><input name="erud_niv" type="text" value='<?= $character === null ? "" : $character->erud_niv; ?>'><input name="erud_stat" type="text" value='<?= $character === null ? "" : $character->erud_stat; ?>'>
                                <input class="stat" id="conn_b1" type="text" value='<?= $character === null ? "" : $character->conn_b1; ?>'><input name="conn_b1_niv" type="text" value='<?= $character === null ? "" : $character->conn_b1_niv; ?>'><input name="conn_b1_stat" type="text" value='<?= $character === null ? "" : $character->conn_b1_stat; ?>'>
                            <label for="conncult_niv" class="d-stat">Connaissances Culturelles (D)</label><input name="conncult_niv" type="text" value='<?= $character === null ? "" : $character->conncult_niv; ?>'><input name="conncult_stat" type="text" value='<?= $character === null ? "" : $character->conncult_stat; ?>'>
                                <input class="stat" id="conn_b2" type="text" value='<?= $character === null ? "" : $character->conn_b2; ?>'><input name="conn_b2_niv" type="text" value='<?= $character === null ? "" : $character->conn_b2_niv; ?>'><input name="conn_b2_stat" type="text" value='<?= $character === null ? "" : $character->conn_b2_stat; ?>'>
                                <label for="cel_niv" class="stat">Contes et légendes</label><input name="cel_niv" type="text" value='<?= $character === null ? "" : $character->cel_niv; ?>'><input name="cel_stat" type="text" value='<?= $character === null ? "" : $character->cel_stat; ?>'>
                                <input class="stat" id="conn_b3" type="text" value='<?= $character === null ? "" : $character->conn_b3; ?>'></label><input name="conn_b3_niv" type="text" value='<?= $character === null ? "" : $character->conn_b3_niv; ?>'><input name="conn_b3_stat" type="text" value='<?= $character === null ? "" : $character->conn_b3_stat; ?>'>
                                <label for="celo_niv" class="stat">Coutumes et lois</label><input name="celo_niv" type="text" value='<?= $character === null ? "" : $character->celo_niv; ?>'><input name="celo_stat" type="text" value='<?= $character === null ? "" : $character->celo_stat; ?>'>
                                <input class="stat" id="conn_b4" type="text" value='<?= $character === null ? "" : $character->conn_b4; ?>'><input name="conn_b4_niv" type="text" value='<?= $character === null ? "" : $character->conn_b4_niv; ?>'><input name="conn_b4_stat" type="text" value='<?= $character === null ? "" : $character->conn_b4_stat; ?>'>
                                <label for="reli_niv" class="stat">Religions</label><input name="reli_niv" type="text" value='<?= $character === null ? "" : $character->reli_niv; ?>'><input name="reli_stat" type="text" value='<?= $character === null ? "" : $character->reli_stat; ?>'>
                                <input class="stat" id="conn_b5" type="text" value='<?= $character === null ? "" : $character->conn_b5; ?>'><input name="conn_b5_niv" type="text" value='<?= $character === null ? "" : $character->conn_b5_niv; ?>'><input name="conn_b5_stat" type="text" value='<?= $character === null ? "" : $character->conn_b5_stat; ?>'>
                            <label for="conncrea_niv" class="d-stat">Connaissances des créatures (D)</label><input name="conncrea_niv" type="text" value='<?= $character === null ? "" : $character->conncrea_niv; ?>'><input name="conncrea_stat" type="text" value='<?= $character === null ? "" : $character->conncrea_stat; ?>'>
                                <label for="coes_niv" class="stat">Connaissance des esprits</label><input name="coes_niv" type="text" value='<?= $character === null ? "" : $character->coes_niv; ?>'><input name="coes_stat" type="text" value='<?= $character === null ? "" : $character->coes_stat; ?>'>
                                <label for="como_niv" class="stat">Connaissance des monstres</label><input name="como_niv" type="text" value='<?= $character === null ? "" : $character->como_niv; ?>'><input name="como_stat" type="text" value='<?= $character === null ? "" : $character->como_stat; ?>'>
                            <label for="languages_niv" class="d-stat">Languages (D)</label><input name="languages_niv" type="text" value='<?= $character === null ? "" : $character->languages_niv; ?>'><input name="languages_stat" type="text" value='<?= $character === null ? "" : $character->languages_stat; ?>'>
                                <label for="bp_niv" class="stat">Beau parleur</label><input name="bp_niv" type="text" value='<?= $character === null ? "" : $character->bp_niv; ?>'><input name="bp_stat" type="text" value='<?= $character === null ? "" : $character->bp_stat; ?>'>
                                <label for="calc_niv" class="stat">Calcul</label><input name="calc_niv" type="text" value='<?= $character === null ? "" : $character->calc_niv; ?>'><input name="calc_stat" type="text" value='<?= $character === null ? "" : $character->calc_stat; ?>'>
                                <label for="corr_niv" class="stat">Corruption</label><input name="corr_niv" type="text" value='<?= $character === null ? "" : $character->corr_niv; ?>'><input name="corr_stat" type="text" value='<?= $character === null ? "" : $character->corr_stat; ?>'>
                                <label for="langetr_niv" class="stat">Langue étrangère</label><input name="langetr_niv" type="text" value='<?= $character === null ? "" : $character->langetr_niv; ?>'><input name="langetr_stat" type="text" value='<?= $character === null ? "" : $character->langetr_stat; ?>'>
                                <input class="stat" id="conn_b6" type="text" value='<?= $character === null ? "" : $character->conn_b6; ?>'><input name="conn_b6_niv" type="text" value='<?= $character === null ? "" : $character->conn_b6_niv; ?>'><input name="conn_b6_stat" type="text" value='<?= $character === null ? "" : $character->conn_b6_stat; ?>'>
                                <label for="langmat_niv" class="stat">Langue maternelle</label><input name="langmat_niv" type="text" value='<?= $character === null ? "" : $character->langmat_niv; ?>'><input name="langmat_stat" type="text" value='<?= $character === null ? "" : $character->langmat_stat; ?>'>
                                <label for="lee_niv" class="stat">Lire et écrire</label><input name="lee_niv" type="text" value='<?= $character === null ? "" : $character->lee_niv; ?>'><input name="lee_stat" type="text" value='<?= $character === null ? "" : $character->lee_stat; ?>'>
                                <input class="stat" id="conn_b7" type="text" value='<?= $character === null ? "" : $character->conn_b7; ?>'><input name="conn_b7_niv" type="text" value='<?= $character === null ? "" : $character->conn_b7_niv; ?>'><input name="conn_b7_stat" type="text" value='<?= $character === null ? "" : $character->conn_b7_stat; ?>'>
                    </fieldset>
                    <fieldset id="divertissement-form" class="competence-forms" form="character-sheet">
                        <label for="divertissement" class="competence">Divertissement</label><p></p><input name="divertissement" type="text" value='<?= $character === null ? "" : $character->divertissement; ?>'>
                            <label for="jeux_niv" class="d-stat">Jeux (D)</label><input name="jeux_niv" type="text" value='<?= $character === null ? "" : $character->jeux_niv; ?>'><input name="jeux_stat" type="text" value='<?= $character === null ? "" : $character->jeux_stat; ?>'>
                                <label for="gdj_niv" class="stat">Grand joueur</label><input name="gdj_niv" type="text" value='<?= $character === null ? "" : $character->gdj_niv; ?>'><input name="gdj_stat" type="text" value='<?= $character === null ? "" : $character->gdj_stat; ?>'>
                                <label for="strj_niv" class="stat">Stratège de jeux</label><input name="strj_niv" type="text" value='<?= $character === null ? "" : $character->strj_niv; ?>'><input name="strj_stat" type="text" value='<?= $character === null ? "" : $character->strj_stat; ?>'>
                                <label for="trich_niv" class="stat">Tricheur</label><input name="trich_niv" type="text" value='<?= $character === null ? "" : $character->trich_niv; ?>'><input name="trich_stat" type="text" value='<?= $character === null ? "" : $character->trich_stat; ?>'>
                            <label for="mued_niv" class="d-stat">Musique et dance (D)</label><input name="mued_niv" type="text" value='<?= $character === null ? "" : $character->mued_niv; ?>'><input name="mued_stat" type="text" value='<?= $character === null ? "" : $character->mued_stat; ?>'>
                                <label for="ceidm_niv" class="stat">Chants et intr. de musique</label><input name="ceidm_niv" type="text" value='<?= $character === null ? "" : $character->ceidm_niv; ?>'><input name="ceidm_stat" type="text" value='<?= $character === null ? "" : $character->ceidm_stat; ?>'>
                                <label for="danse_niv" class="stat">Danse</label><input name="danse_niv" type="text" value='<?= $character === null ? "" : $character->danse_niv; ?>'><input name="danse_stat" type="text" value='<?= $character === null ? "" : $character->danse_stat; ?>'>
                            <label for="narr_niv" class="d-stat">Narration (D)</label><input name="narr_niv" type="text" value='<?= $character === null ? "" : $character->narr_niv; ?>'><input name="narr_stat" type="text" value='<?= $character === null ? "" : $character->narr_stat; ?>'>
                                <label for="calomn_niv" class="stat">Calomnies</label><input name="calomn_niv" type="text" value='<?= $character === null ? "" : $character->calomn_niv; ?>'><input name="calomn_stat" type="text" value='<?= $character === null ? "" : $character->calomn_stat; ?>'>
                                <label for="come_niv" class="stat">Comédie</label><input name="come_niv" type="text" value='<?= $character === null ? "" : $character->come_niv; ?>'><input name="come_stat" type="text" value='<?= $character === null ? "" : $character->come_stat; ?>'>
                    </fieldset>
                    <fieldset id="foi-form" class="competence-forms" form="character-sheet">
                        <label for="foi" class="competence">Foi</label><p></p><input name="foi" type="text" value='<?= $character === null ? "" : $character->foi; ?>'>
                            <label for="concrel_niv" class="d-stat">Concentration Religieuse (D)</label><input name="concrel_niv" type="text" value='<?= $character === null ? "" : $character->concrel_niv; ?>'><input name="concrel_stat" type="text" value='<?= $character === null ? "" : $character->concrel_stat; ?>'>
                                <label for="invoinst_niv" class="stat">Invocation instantanée</label><input name="invoinst_niv" type="text" value='<?= $character === null ? "" : $character->invoinst_niv; ?>'><input name="invoinst_stat" type="text" value='<?= $character === null ? "" : $character->invoinst_stat; ?>'>
                                <label for="rigou_niv" class="stat">Rigoureux</label><input name="rigou_niv" type="text" value='<?= $character === null ? "" : $character->rigou_niv; ?>'><input name="rigou_stat" type="text" value='<?= $character === null ? "" : $character->rigou_stat; ?>'>
                                <label for="sere_niv" class="stat">Sérénité</label><input name="sere_niv" type="text" value='<?= $character === null ? "" : $character->sere_niv; ?>'><input name="sere_stat" type="text" value='<?= $character === null ? "" : $character->sere_stat; ?>'>
                                <label for="transc_niv" class="stat">Transcendance</label><input name="transc_niv" type="text" value='<?= $character === null ? "" : $character->transc_niv; ?>'><input name="transc_stat" type="text" value='<?= $character === null ? "" : $character->transc_stat; ?>'>
                            <label for="invoc_niv" class="d-stat">Invocation (D)</label><input name="invoc_niv" type="text" value='<?= $character === null ? "" : $character->invoc_niv; ?>'><input name="invoc_stat" type="text" value='<?= $character === null ? "" : $character->invoc_stat; ?>'>
                                <label for="buide_niv" class="stat">Bruide</label><input name="buide_niv" type="text" value='<?= $character === null ? "" : $character->buide_niv; ?>'><input name="buide_stat" type="text" value='<?= $character === null ? "" : $character->buide_stat; ?>'>
                                <label for="forthu_niv" class="stat">Forgeage thuul</label><input name="forthu_niv" type="text" value='<?= $character === null ? "" : $character->forthu_niv; ?>'><input name="forthu_stat" type="text" value='<?= $character === null ? "" : $character->forthu_stat; ?>'>
                                <label for="gavlien_niv" class="stat">Gavlien</label><input name="gavlien_niv" type="text" value='<?= $character === null ? "" : $character->gavlien_niv; ?>'><input name="gavlien_stat" type="text" value='<?= $character === null ? "" : $character->gavlien_stat; ?>'>
                                <label for="ihana_niv" class="stat">Ihana</label><input name="ihana_niv" type="text" value='<?= $character === null ? "" : $character->ihana_niv; ?>'><input name="ihana_stat" type="text" value='<?= $character === null ? "" : $character->ihana_stat; ?>'>
                                <label for="noaj_niv" class="stat">Noaj</label><input name="noaj_niv" type="text" value='<?= $character === null ? "" : $character->noaj_niv; ?>'><input name="noaj_stat" type="text" value='<?= $character === null ? "" : $character->noaj_stat; ?>'>
                                <label for="storm_niv" class="stat">Stormikjalt</label><input name="storm_niv" type="text" value='<?= $character === null ? "" : $character->storm_niv; ?>'><input name="storm_stat" type="text" value='<?= $character === null ? "" : $character->storm_stat; ?>'>
                            <label for="pouvdiv_niv" class="d-stat">Pouvoirs divins (D)</label><input name="pouvdiv_niv" type="text" value='<?= $character === null ? "" : $character->pouvdiv_niv; ?>'><input name="pouvdiv_stat" type="text" value='<?= $character === null ? "" : $character->pouvdiv_stat; ?>'>
                                <label for="devo_niv" class="stat">Dévotion</label><input name="devo_niv" type="text" value='<?= $character === null ? "" : $character->devo_niv; ?>'><input name="devo_stat" type="text" value='<?= $character === null ? "" : $character->devo_stat; ?>'>
                                <label for="puiss_niv" class="stat">Puissant</label><input name="puiss_niv" type="text" value='<?= $character === null ? "" : $character->puiss_niv; ?>'><input name="puiss_stat" type="text" value='<?= $character === null ? "" : $character->puiss_stat; ?>'>
                    </fieldset>
                    <fieldset id="vitner-form" class="competence-forms" form="character-sheet">
                        <label for="vitner" class="competence">Maîtrise du Vitner</label><p></p><input name="vitner" type="text" value='<?= $character === null ? "" : $character->vitner; ?>'>
                            <label for="appvitner_niv" class="d-stat">Appel du vitner (D)</label><input name="appvitner_niv" type="text" value='<?= $character === null ? "" : $character->appvitner_niv; ?>'><input name="appvitner_stat" type="text" value='<?= $character === null ? "" : $character->appvitner_stat; ?>'>
                                <label for="habvitner_niv" class="stat">Habitude du vitner</label><input name="habvitner_niv" type="text" value='<?= $character === null ? "" : $character->habvitner_niv; ?>'><input name="habvitner_stat" type="text" value='<?= $character === null ? "" : $character->habvitner_stat; ?>'>
                                <label for="hwat_niv" class="stat">Hwatalja</label><input name="hwat_niv" type="text" value='<?= $character === null ? "" : $character->hwat_niv; ?>'><input name="hwat_stat" type="text" value='<?= $character === null ? "" : $character->hwat_stat; ?>'>
                                <label for="mork_niv" class="stat">Morkvitalja</label><input name="mork_niv" type="text" value='<?= $character === null ? "" : $character->mork_niv; ?>'><input name="mork_stat" type="text" value='<?= $character === null ? "" : $character->mork_stat; ?>'>
                                <label for="vaag_niv" class="stat">Vaagritalja</label><input name="vaag_niv" type="text" value='<?= $character === null ? "" : $character->vaag_niv; ?>'><input name="vaag_stat" type="text" value='<?= $character === null ? "" : $character->vaag_stat; ?>'>
                            <label for="conctiss_niv" class="d-stat">Concentration du tisseur (D)</label><input name="conctiss_niv" type="text" value='<?= $character === null ? "" : $character->conctiss_niv; ?>'><input name="conctiss_stat" type="text" value='<?= $character === null ? "" : $character->conctiss_stat; ?>'>
                                <label for="puissvitner_niv" class="stat">Puissance</label><input name="puissvitner_niv" type="text" value='<?= $character === null ? "" : $character->puissvitner_niv; ?>'><input name="puissvitner_stat" type="text" value='<?= $character === null ? "" : $character->puissvitner_stat; ?>'>
                                <label for="renfo_niv" class="stat">Renforcement</label><input name="renfo_niv" type="text" value='<?= $character === null ? "" : $character->renfo_niv; ?>'><input name="renfo_stat" type="text" value='<?= $character === null ? "" : $character->renfo_stat; ?>'>
                                <label for="tissatt_niv" class="stat">Tissage attentif</label><input name="tissatt_niv" type="text" value='<?= $character === null ? "" : $character->tissatt_niv; ?>'><input name="tissatt_stat" type="text" value='<?= $character === null ? "" : $character->tissatt_stat; ?>'>
                            <label for="modvitner_niv" class="d-stat">Modelage du vitner (D)</label><input name="modvitner_niv" type="text" value='<?= $character === null ? "" : $character->modvitner_niv; ?>'><input name="modvitner_stat" type="text" value='<?= $character === null ? "" : $character->modvitner_stat; ?>'>
                                <label for="gakta_niv" class="stat">Gakta</label><input name="gakta_niv" type="text" value='<?= $character === null ? "" : $character->gakta_niv; ?>'><input name="gakta_stat" type="text" value='<?= $character === null ? "" : $character->gakta_stat; ?>'>
                                <label for="runes_niv" class="stat">Runes de vitner</label><input name="runes_niv" type="text" value='<?= $character === null ? "" : $character->runes_niv; ?>'><input name="runes_stat" type="text" value='<?= $character === null ? "" : $character->runes_stat; ?>'>
                                <label for="sejda_niv" class="stat">Sejda</label><input name="sejda_niv" type="text" value='<?= $character === null ? "" : $character->sejda_niv; ?>'><input name="sejda_stat" type="text" value='<?= $character === null ? "" : $character->sejda_stat; ?>'>
                                <label for="vynia_niv" class="stat">Vynia</label><input name="vynia_niv" type="text" value='<?= $character === null ? "" : $character->vynia_niv; ?>'><input name="vynia_stat" type="text" value='<?= $character === null ? "" : $character->vynia_stat; ?>'>
                    </fieldset>
                    <fieldset id="nature-form" class="competence-forms" form="character-sheet">
                        <label for="nature" class="competence">Nature</label><p></p><input name="nature" type="text" value='<?= $character === null ? "" : $character->nature; ?>'>
                            <label for="connnat_niv" class="d-stat">Connaissances de la nature (D)</label><input name="connnat_niv" type="text" value='<?= $character === null ? "" : $character->connnat_niv; ?>'><input name="connnat_stat" type="text" value='<?= $character === null ? "" : $character->connnat_stat; ?>'>
                                <label for="ada_niv" class="stat">Ami des animaux</label><input name="ada_niv" type="text" value='<?= $character === null ? "" : $character->ada_niv; ?>'><input name="ada_stat" type="text" value='<?= $character === null ? "" : $character->ada_stat; ?>'>
                                <label for="bota_niv" class="stat">Botanique</label><input name="bota_niv" type="text" value='<?= $character === null ? "" : $character->bota_niv; ?>'><input name="bota_stat" type="text" value='<?= $character === null ? "" : $character->bota_stat; ?>'>
                                <label for="meteo_niv" class="stat">Météorologie</label><input name="meteo_niv" type="text" value='<?= $character === null ? "" : $character->meteo_niv; ?>'><input name="meteo_stat" type="text" value='<?= $character === null ? "" : $character->meteo_stat; ?>'>
                                <label for="zool_niv" class="stat">Zoologie</label><input name="zool_niv" type="text" value='<?= $character === null ? "" : $character->zool_niv; ?>'><input name="zool_stat" type="text" value='<?= $character === null ? "" : $character->zool_stat; ?>'>
                            <label for="expdc_niv" class="d-stat">Expérience de la chasse (D)</label><input name="expdc_niv" type="text" value='<?= $character === null ? "" : $character->expdc_niv; ?>'><input name="expdc_stat" type="text" value='<?= $character === null ? "" : $character->expdc_stat; ?>'>
                                <label for="cue_niv" class="stat">Chasser une espêce</label><input name="cue_niv" type="text" value='<?= $character === null ? "" : $character->cue_niv; ?>'><input name="cue_stat" type="text" value='<?= $character === null ? "" : $character->cue_stat; ?>'>
                                <label for="cep_niv" class="stat">Chasser et pêcher</label><input name="cep_niv" type="text" value='<?= $character === null ? "" : $character->cep_niv; ?>'><input name="cep_stat" type="text" value='<?= $character === null ? "" : $character->cep_stat; ?>'>
                                <label for="ded_niv" class="stat">Découper et dépecer</label><input name="ded_niv" type="text" value='<?= $character === null ? "" : $character->ded_niv; ?>'><input name="ded_stat" type="text" value='<?= $character === null ? "" : $character->ded_stat; ?>'>
                                <label for="pist_niv" class="stat">Pister</label><input name="pist_niv" type="text" value='<?= $character === null ? "" : $character->pist_niv; ?>'><input name="pist_stat" type="text" value='<?= $character === null ? "" : $character->pist_stat; ?>'>
                                <label for="sdln_niv" class="stat">Signes de la nature</label><input name="sdln_niv" type="text" value='<?= $character === null ? "" : $character->sdln_niv; ?>'><input name="sdln_stat" type="text" value='<?= $character === null ? "" : $character->sdln_stat; ?>'>
                            <label for="geog_niv" class="d-stat">Géographie (D)</label><input name="geog_niv" type="text" value='<?= $character === null ? "" : $character->geog_niv; ?>'><input name="geog_stat" type="text" value='<?= $character === null ? "" : $character->geog_stat; ?>'>
                                <input class="stat" id="nat_b1" type="text" value='<?= $character === null ? "Conn. des cités" : $character->nat_b5_niv; ?>'><input name="nat_b1_niv" type="text" value='<?= $character === null ? "" : $character->nat_b1_niv; ?>'><input name="nat_b1_stat" type="text" value='<?= $character === null ? "" : $character->nat_b1_stat; ?>'>
                                <input class="stat" id="nat_b2" type="text" value='<?= $character === null ? "Conn. des mers" : $character->nat_b5_niv; ?>'><input name="nat_b2_niv" type="text" value='<?= $character === null ? "" : $character->nat_b2_niv; ?>'><input name="nat_b2_stat" type="text" value='<?= $character === null ? "" : $character->nat_b2_stat; ?>'>
                                <input class="stat" id="nat_b3" type="text" value='<?= $character === null ? "Conn. régionale" : $character->nat_b5_niv; ?>'><input name="nat_b3_niv" type="text" value='<?= $character === null ? "" : $character->nat_b3_niv; ?>'><input name="nat_b3_stat" type="text" value='<?= $character === null ? "" : $character->nat_b3_stat; ?>'>
                                <label for="oec_niv" class="stat">Orientation et cartographie</label><input name="oec_niv" type="text" value='<?= $character === null ? "" : $character->oec_niv; ?>'><input name="oec_stat" type="text" value='<?= $character === null ? "" : $character->oec_stat; ?>'>
                            <label for="marin_niv" class="d-stat">Marin (D)</label><input name="marin_niv" type="text" value='<?= $character === null ? "" : $character->marin_niv; ?>'><input name="marin_stat" type="text" value='<?= $character === null ? "" : $character->marin_stat; ?>'>
                                <label for="mate_niv" class="stat">Matelot</label><input name="mate_niv" type="text" value='<?= $character === null ? "" : $character->mate_niv; ?>'><input name="mate_stat" type="text" value='<?= $character === null ? "" : $character->mate_stat; ?>'>
                                <label for="navi_niv" class="stat">Navigation</label><input name="navi_niv" type="text" value='<?= $character === null ? "" : $character->navi_niv; ?>'><input name="navi_stat" type="text" value='<?= $character === null ? "" : $character->navi_stat; ?>'>
                            <label for="survie_niv" class="d-stat">Survie (D)</label><input name="survie_niv" type="text" value='<?= $character === null ? "" : $character->survie_niv; ?>'><input name="survie_stat" type="text" value='<?= $character === null ? "" : $character->survie_stat; ?>'>
                                <label for="camp_niv" class="stat">Campement</label><input name="camp_niv" type="text" value='<?= $character === null ? "" : $character->camp_niv; ?>'><input name="camp_stat" type="text" value='<?= $character === null ? "" : $character->camp_stat; ?>'>
                                <label for="cdenv_niv" class="stat">Conn. de l'environnement</label><input name="cdenv_niv" type="text" value='<?= $character === null ? "" : $character->cdenv_niv; ?>'><input name="cdenv_stat" type="text" value='<?= $character === null ? "" : $character->cdenv_stat; ?>'>
                                <label for="endur_niv" class="stat">Endurci</label><input name="endur_niv" type="text" value='<?= $character === null ? "" : $character->endur_niv; ?>'><input name="endur_stat" type="text" value='<?= $character === null ? "" : $character->endur_stat; ?>'>
                                <input class="stat" id="nat_b4" type="text" value="Exploration"><input name="nat_b4_niv" type="text" value='<?= $character === null ? "" : $character->nat_b4_niv; ?>'><input name="nat_b4_stat" type="text" value='<?= $character === null ? "" : $character->nat_b4_stat; ?>'>
                                <input class="stat" id="nat_b5" type="text"><input name="nat_b5_niv" type="text" value='<?= $character === null ? "" : $character->nat_b5_niv; ?>'><input name="nat_b5_stat" type="text" value='<?= $character === null ? "" : $character->nat_b5_stat; ?>'>
                    </fieldset>
                    <fieldset id="savoirfaire-form" class="competence-forms" form="character-sheet">
                        <label for="savoirfaire" class="competence">Savoir-faire</label><p></p><input name="savoirfaire" type="text" value='<?= $character === null ? "" : $character->savoirfaire; ?>'>
                            <label for="artis_niv" class="d-stat">Artisanat (D)</label><input name="artis_niv" type="text" value='<?= $character === null ? "" : $character->artis_niv; ?>'><input name="artis_stat" type="text" value='<?= $character === null ? "" : $character->artis_stat; ?>'>
                                <label for="contref_niv" class="stat">Contrefaçon</label><input name="contref_niv" type="text" value='<?= $character === null ? "" : $character->contref_niv; ?>'><input name="contref_stat" type="text" value='<?= $character === null ? "" : $character->contref_stat; ?>'>
                                <label for="matdu_niv" class="stat">Matériaux durs</label><input name="matdu_niv" type="text" value='<?= $character === null ? "" : $character->matdu_niv; ?>'><input name="matdu_stat" type="text" value='<?= $character === null ? "" : $character->matdu_stat; ?>'>
                                <label for="matsoup_niv" class="stat">Matériaux souples</label><input name="matsoup_niv" type="text" value='<?= $character === null ? "" : $character->matsoup_niv; ?>'><input name="matsoup_stat" type="text" value='<?= $character === null ? "" : $character->matsoup_stat; ?>'>
                            <label for="commer_niv" class="d-stat">Commerce (D)</label><input name="commer_niv" type="text" value='<?= $character === null ? "" : $character->commer_niv; ?>'><input name="commer_stat" type="text" value='<?= $character === null ? "" : $character->commer_stat; ?>'>
                                <label for="barb_niv" class="stat">Barbier</label><input name="barb_niv" type="text" value='<?= $character === null ? "" : $character->barb_niv; ?>'><input name="barb_stat" type="text" value='<?= $character === null ? "" : $character->barb_stat; ?>'>
                                <label for="brass_niv" class="stat">Brasseur</label><input name="brass_niv" type="text" value='<?= $character === null ? "" : $character->brass_niv; ?>'><input name="brass_stat" type="text" value='<?= $character === null ? "" : $character->brass_stat; ?>'>
                                <label for="cuisi_niv" class="stat">Cuisinier</label><input name="cuisi_niv" type="text" value='<?= $character === null ? "" : $character->cuisi_niv; ?>'><input name="cuisi_stat" type="text" value='<?= $character === null ? "" : $character->cuisi_stat; ?>'>
                                <label for="march_niv" class="stat">Marchand</label><input name="march_niv" type="text" value='<?= $character === null ? "" : $character->march_niv; ?>'><input name="march_stat" type="text" value='<?= $character === null ? "" : $character->march_stat; ?>'>
                                <label for="pays_niv" class="stat">Paysan</label><input name="pays_niv" type="text" value='<?= $character === null ? "" : $character->pays_niv; ?>'><input name="pays_stat" type="text" value='<?= $character === null ? "" : $character->pays_stat; ?>'>
                            <label for="dirig_niv" class="d-stat">Diriger (D)</label><input name="dirig_niv" type="text" value='<?= $character === null ? "" : $character->dirig_niv; ?>'><input name="dirig_stat" type="text" value='<?= $character === null ? "" : $character->dirig_stat; ?>'>
                                <label for="admini_niv" class="stat">Administrer</label><input name="admini_niv" type="text" value='<?= $character === null ? "" : $character->admini_niv; ?>'><input name="admini_stat" type="text" value='<?= $character === null ? "" : $character->admini_stat; ?>'>
                                <label for="commander_niv" class="stat">Commander</label><input name="commander_niv" type="text" value='<?= $character === null ? "" : $character->commander_niv; ?>'><input name="commander_stat" type="text" value='<?= $character === null ? "" : $character->commander_stat; ?>'>
                                <label for="soetre_niv" class="d-stat">Soins et remèdes (D)</label><input name="soetre_niv" type="text" value='<?= $character === null ? "" : $character->soetre_niv; ?>'><input name="soetre_stat" type="text" value='<?= $character === null ? "" : $character->soetre_stat; ?>'>
                                <label for="exetpoi_niv" class="stat">Extrais et poisons</label><input name="exetpoi_niv" type="text" value='<?= $character === null ? "" : $character->exetpoi_niv; ?>'><input name="exetpoi_stat" type="text" value='<?= $character === null ? "" : $character->exetpoi_stat; ?>'>
                                <label for="pses_niv" class="stat">Premiers secours et soins</label><input name="pses_niv" type="text" value='<?= $character === null ? "" : $character->pses_niv; ?>'><input name="pses_stat" type="text" value='<?= $character === null ? "" : $character->pses_stat; ?>'>
                    </fieldset>
                </fieldset>
            </div>
            <div id="character-inventory" class="tabcontent">
                <fieldset id="toprow" form="character-sheet">
                    <fieldset id="armure">
                        <legend>Armure / Bouclier</legend>
                        <p>Type</p><p>VP</p><p>VI</p><p>ENC</p><p>MM</p><p>MI</p>
                        <input name="armure_t1" type="text" value='<?= $character === null ? "" : $character->armure_t1; ?>'>
                        <input name="armure_vp1" type="number" value='<?= $character === null ? "" : $character->armure_vp1; ?>'>
                        <input name="armure_vi1" type="number" value='<?= $character === null ? "" : $character->armure_vi1; ?>'>
                        <input name="armure_enc1" type="number" value='<?= $character === null ? "" : $character->armure_enc1; ?>'>
                        <input name="armure_mm1" type="number" value='<?= $character === null ? "" : $character->armure_mm1; ?>'>
                        <input name="armure_mi1" type="number" value='<?= $character === null ? "" : $character->armure_mi1; ?>'>
                        <input name="armure_t2" type="text" value='<?= $character === null ? "" : $character->armure_t2; ?>'>
                        <input name="armure_vp2" type="number" value='<?= $character === null ? "" : $character->armure_vp2; ?>'>
                        <input name="armure_vi2" type="number" value='<?= $character === null ? "" : $character->armure_vi2; ?>'>
                        <input name="armure_enc2" type="number" value='<?= $character === null ? "" : $character->armure_enc2; ?>'>
                        <input name="armure_mm2" type="number" value='<?= $character === null ? "" : $character->armure_mm2; ?>'>
                        <input name="armure_mi2" type="number" value='<?= $character === null ? "" : $character->armure_mi2; ?>'>
                        <input name="armure_t3" type="text" value='<?= $character === null ? "" : $character->armure_t3; ?>'>
                        <input name="armure_vp3" type="number" value='<?= $character === null ? "" : $character->armure_vp3; ?>'>
                        <input name="armure_vi3" type="number" value='<?= $character === null ? "" : $character->armure_vi3; ?>'>
                        <input name="armure_enc3" type="number" value='<?= $character === null ? "" : $character->armure_enc3; ?>'>
                        <input name="armure_mm3" type="number" value='<?= $character === null ? "" : $character->armure_mm3; ?>'>
                        <input name="armure_mi3" type="number" value='<?= $character === null ? "" : $character->armure_mi3; ?>'>
                    </fieldset>
                    <fieldset id="equipement">
                        <legend>Equipement</legend>
                        <textarea name="equipment" rows="15" cols="70"><?= $character === null ? "" : $character->equipment; ?></textarea>
                    </fieldset>
                </fieldset>
                <fieldset id="armes" form="character-sheet">
                    <legend>Armes</legend>
                    <p>Type</p><p>AA</p><p>MI</p><p>VP</p><p>VI</p><p>Dégats</p><p>Portée Courte</p><p>Portée Longue</p><p>infos</p>
                    <input name="arme_t1" type="text" value='<?= $character === null ? "" : $character->arme_t1; ?>'>
                    <input name="arme_aa1" type="number" value='<?= $character === null ? "" : $character->arme_aa1; ?>'>
                    <input name="arme_mi1" type="number" value='<?= $character === null ? "" : $character->arme_mi1; ?>'>
                    <input name="arme_vp1" type="number" value='<?= $character === null ? "" : $character->arme_vp1; ?>'>
                    <input name="arme_vi1" type="number" value='<?= $character === null ? "" : $character->arme_vi1; ?>'>
                    <input name="arme_d1" type="text" value='<?= $character === null ? "" : $character->arme_d1; ?>'>
                    <input name="arme_pc1" type="number" value='<?= $character === null ? "" : $character->arme_pc1; ?>'>
                    <input name="arme_pl1" type="number" value='<?= $character === null ? "" : $character->arme_pl1; ?>'>
                    <input name="arme_info1" type="text" value='<?= $character === null ? "" : $character->arme_info1; ?>'>
                    <input name="arme_t2" type="text" value='<?= $character === null ? "" : $character->arme_t2; ?>'>
                    <input name="arme_aa2" type="number" value='<?= $character === null ? "" : $character->arme_aa2; ?>'>
                    <input name="arme_mi2" type="number" value='<?= $character === null ? "" : $character->arme_mi2; ?>'>
                    <input name="arme_vp2" type="number" value='<?= $character === null ? "" : $character->arme_vp2; ?>'>
                    <input name="arme_vi2" type="number" value='<?= $character === null ? "" : $character->arme_vi2; ?>'>
                    <input name="arme_d2" type="text" value='<?= $character === null ? "" : $character->arme_d2; ?>'>
                    <input name="arme_pc2" type="number" value='<?= $character === null ? "" : $character->arme_pc2; ?>'>
                    <input name="arme_pl2" type="number" value='<?= $character === null ? "" : $character->arme_pl2; ?>'>
                    <input name="arme_info2" type="text" value='<?= $character === null ? "" : $character->arme_info2; ?>'>
                    <input name="arme_t3" type="text" value='<?= $character === null ? "" : $character->arme_t3; ?>'>
                    <input name="arme_aa3" type="number" value='<?= $character === null ? "" : $character->arme_aa3; ?>'>
                    <input name="arme_mi3" type="number" value='<?= $character === null ? "" : $character->arme_mi3; ?>'>
                    <input name="arme_vp3" type="number" value='<?= $character === null ? "" : $character->arme_vp3; ?>'>
                    <input name="arme_vi3" type="number" value='<?= $character === null ? "" : $character->arme_vi3; ?>'>
                    <input name="arme_d3" type="text" value='<?= $character === null ? "" : $character->arme_d3; ?>'>
                    <input name="arme_pc3" type="number" value='<?= $character === null ? "" : $character->arme_pc3; ?>'>
                    <input name="arme_pl3" type="number" value='<?= $character === null ? "" : $character->arme_pl3; ?>'>
                    <input name="arme_info3" type="text" value='<?= $character === null ? "" : $character->arme_info3; ?>'>
                    <input name="arme_t4" type="text" value='<?= $character === null ? "" : $character->arme_t4; ?>'>
                    <input name="arme_aa4" type="number" value='<?= $character === null ? "" : $character->arme_aa4; ?>'>
                    <input name="arme_mi4" type="number" value='<?= $character === null ? "" : $character->arme_mi4; ?>'>
                    <input name="arme_vp4" type="number" value='<?= $character === null ? "" : $character->arme_vp4; ?>'>
                    <input name="arme_vi4" type="number" value='<?= $character === null ? "" : $character->arme_vi4; ?>'>
                    <input name="arme_d4" type="text" value='<?= $character === null ? "" : $character->arme_d4; ?>'>
                    <input name="arme_pc4" type="number" value='<?= $character === null ? "" : $character->arme_pc4; ?>'>
                    <input name="arme_pl4" type="number" value='<?= $character === null ? "" : $character->arme_pl4; ?>'>
                    <input name="arme_info4" type="text" value='<?= $character === null ? "" : $character->arme_info4; ?>'>
                </fieldset>
            </div>
            <div id="character-state" class="tabcontent">
                <fieldset id="initiativedebase" form="character-sheet">
                    <legend>Initiative de base</legend>
                    <p>Modificateur</p><p>init</p>
                    <input name="modinit1" type="text" value='<?= $character === null ? "" : $character->modinit1; ?>'><input name="modinit1v" type="number" value='<?= $character === null ? "" : $character->modinit1v; ?>'>
                    <input name="modinit2" type="text" value='<?= $character === null ? "" : $character->modinit2; ?>'><input name="modinit2v" type="number" value='<?= $character === null ? "" : $character->modinit2v; ?>'>
                    <input name="modinit3" type="text" value='<?= $character === null ? "" : $character->modinit3; ?>'><input name="modinit3v" type="number" value='<?= $character === null ? "" : $character->modinit3v; ?>'>
                    <input name="modinit4" type="text" value='<?= $character === null ? "" : $character->modinit4; ?>'><input name="modinit4v" type="number" value='<?= $character === null ? "" : $character->modinit4v; ?>'>
                    <input name="modinit5" type="text" value='<?= $character === null ? "" : $character->modinit5; ?>'><input name="modinit5v" type="number" value='<?= $character === null ? "" : $character->modinit5v; ?>'>
                </fieldset>
                <fieldset id="pointsdecombat" form="character-sheet">
                    <legend>Points de combat</legend>
                    <fieldset id="pdc-form">
                        <label for="libres">Libres <input type="number" name="libres" value='<?= $character === null ? "" : $character->libres; ?>'></label>
                        <label for="attepar">Attaque et Parade<input type="number" name="attepar" value='<?= $character === null ? "" : $character->attepar; ?>'></label>
                        <label for="actdc">Actions de combat<input type="number" name="actdc" value='<?= $character === null ? "" : $character->actdc; ?>'></label>
                    </fieldset>
                    <fieldset id="pdc-arme">
                        <label for="armep">Armé <input type="number" id="armep"></label>
                        <fieldset id="armespdc-form">
                            <label for="arba_pdc" class="stat">Arbalète</label><input name="arba_pdc" type="text" value='<?= $character === null ? "" : $character->arba_pdc; ?>'><p></p>
                            <label for="aef_pdc" class="stat">Arcs et frondes</label><input name="aef_pdc" type="text" value='<?= $character === null ? "" : $character->aef_pdc; ?>'><p></p>
                            <label for="aadm_pdc" class="stat">Armes à deux mains</label><input name="aadm_pdc" type="text" value='<?= $character === null ? "" : $character->aadm_pdc; ?>'><p></p>
                            <label for="adl_pdc" class="stat">Armes de lancer</label><input name="adl_pdc" type="text" value='<?= $character === null ? "" : $character->adl_pdc; ?>'><input name="adl2_pdc" type="text" value='<?= $character === null ? "" : $character->adl2_pdc; ?>'>
                            <label for="alaum_pdc" class="stat">Armes légères à une main</label><input name="alaum_pdc" type="text" value='<?= $character === null ? "" : $character->alaum_pdc; ?>'><input name="alaum2_pdc" type="text" value='<?= $character === null ? "" : $character->alaum2_pdc; ?>'>
                            <label for="allaum_pdc" class="stat">Armes lourdes à une main</label><input name="allaum_pdc" type="text" value='<?= $character === null ? "" : $character->allaum_pdc; ?>'><input name="allaum2_pdc" type="text" value='<?= $character === null ? "" : $character->allaum2_pdc; ?>'>
                            <label for="pdb_pdc" class="stat">Porteur de bouclier</label><input name="pdb_pdc" type="text" value='<?= $character === null ? "" : $character->pdb_pdc; ?>'><p></p>
                        </fieldset>
                        <fieldset id="amn-bform">
                            <fieldset id="amn-pts">
                                <label for="amn_pdc">À mains nues <input type="number" name="amn_pdc" id="amn_pdc" value='<?= $character === null ? "" : $character->amn_pdc; ?>'></label>
                                <fieldset id="amn-form">
                                    <label for="baga_pdc" class="stat">Bagarre</label><input name="baga_pdc" type="text" value='<?= $character === null ? "" : $character->baga_pdc; ?>'>
                                    <label for="lut_pdc" class="stat">Lutte</label><input name="lut_pdc" type="text" value='<?= $character === null ? "" : $character->lut_pdc; ?>'>
                                </fieldset>
                            </fieldset>
                            <p>Combinaisons d'attaques</p>
                            <fieldset id="comb-attaque">
                                <input name="cda1_pdc" type="text" value='<?= $character === null ? "" : $character->cda1_pdc; ?>'>
                                <input name="cda2_pdc" type="text" value='<?= $character === null ? "" : $character->cda2_pdc; ?>'>
                                <input name="cda3_pdc" type="text" value='<?= $character === null ? "" : $character->cda3_pdc; ?>'>
                            </fieldset>
                        </fieldset>
                    </fieldset>
                </fieldset>
                <fieldset id="pointsdesante" form="character-sheet">
                    <legend>Points de santé</legend>
                    <label for="pds">Points de santé<input type="number" name="pds" value='<?= $character === null ? "" : $character->pds; ?>'></label>
                    <label for="gn">Guérison naturelle<input type="number" name="gn" value='<?= $character === null ? "" : $character->gn; ?>'></label>
                    <fieldset name="egratigne">
                        <legend>Égratigné</legend>
                        <input type="checkbox" name="egra1" <?= $character->egra1 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra2"<?= $character->egra2 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra3"<?= $character->egra3 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra4"<?= $character->egra4 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra5"<?= $character->egra5 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra6"<?= $character->egra6 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra7"<?= $character->egra7 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra8"<?= $character->egra8 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra9"<?= $character->egra9 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra10"<?= $character->egra10 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra11"<?= $character->egra11 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra12"<?= $character->egra12 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra13"<?= $character->egra13 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra14"<?= $character->egra14 === null ? : "checked"; ?>>
                        <input type="checkbox" name="egra15"<?= $character->egra15 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="blesse">
                        <legend>Blessé</legend>
                        <input type="checkbox" name="bless1"<?= $character->bless1 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless2"<?= $character->bless2 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless3"<?= $character->bless3 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless4"<?= $character->bless4 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless5"<?= $character->bless5 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless6"<?= $character->bless6 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless7"<?= $character->bless7 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless8"<?= $character->bless8 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless9"<?= $character->bless9 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless10"<?= $character->bless10 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless11"<?= $character->bless11 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless12"<?= $character->bless12 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless13"<?= $character->bless13 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless14"<?= $character->bless14 === null ? : "checked"; ?>>
                        <input type="checkbox" name="bless15"<?= $character->bless15 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="gravbless">
                        <legend>Gravement blessé</legend>
                        <input type="checkbox" name="gravbless1"<?= $character->gravbless1 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless2"<?= $character->gravbless2 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless3"<?= $character->gravbless3 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless4"<?= $character->gravbless4 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless5"<?= $character->gravbless5 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless6"<?= $character->gravbless6 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless7"<?= $character->gravbless7 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless8"<?= $character->gravbless8 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless9"<?= $character->gravbless9 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless10"<?= $character->gravbless10 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless11"<?= $character->gravbless11 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless12"<?= $character->gravbless12 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless13"<?= $character->gravbless13 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless14"<?= $character->gravbless14 === null ? : "checked"; ?>>
                        <input type="checkbox" name="gravbless15"<?= $character->gravbless15 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="mortbless">
                        <legend>Mortelement blessé</legend>
                        <input type="checkbox" name="mortbless1"<?= $character->mortbless1 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless2"<?= $character->mortbless2 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless3"<?= $character->mortbless3 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless4"<?= $character->mortbless4 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless5"<?= $character->mortbless5 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless6"<?= $character->mortbless6 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless7"<?= $character->mortbless7 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless8"<?= $character->mortbless8 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless9"<?= $character->mortbless9 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless10"<?= $character->mortbless10 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless11"<?= $character->mortbless11 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless12"<?= $character->mortbless12 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless13"<?= $character->mortbless13 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless14"<?= $character->mortbless14 === null ? : "checked"; ?>>
                        <input type="checkbox" name="mortbless15"<?= $character->mortbless15 === null ? : "checked"; ?>>
                    </fieldset>
                </fieldset>
                <fieldset id="pointsdepeur" form="character-sheet">
                    <legend>Peur</legend>
                    <label for="mdp">Modif. de peur<input type="number" name="mdp" value='<?= $character === null ? "" : $character->mdp; ?>'></label>
                    <fieldset name="angoisse">
                        <legend>Niveau 1 (Angoissé)(±0)</legend>
                            <input type="checkbox" name="angoi1"<?= $character->angoi1 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi2"<?= $character->angoi2 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi3"<?= $character->angoi3 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi4"<?= $character->angoi4 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi5"<?= $character->angoi5 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi6"<?= $character->angoi6 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi7"<?= $character->angoi7 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi8"<?= $character->angoi8 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi9"<?= $character->angoi9 === null ? : "checked"; ?>>
                            <input type="checkbox" name="angoi10"<?= $character->angoi10 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="effray">
                        <legend>Niveau 2 (Effrayé)(-1)</legend>
                            <input type="checkbox" name="effray1"<?= $character->effray1 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray2"<?= $character->effray2 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray3"<?= $character->effray3 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray4"<?= $character->effray4 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray5"<?= $character->effray5 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray6"<?= $character->effray6 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray7"<?= $character->effray7 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray8"<?= $character->effray8 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray9"<?= $character->effray9 === null ? : "checked"; ?>>
                            <input type="checkbox" name="effray10"<?= $character->effray10 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="horri">
                        <legend>Niveau 3 (Horrifié)(-3)</legend>
                            <input type="checkbox" name="horri1"<?= $character->horri1 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri2"<?= $character->horri2 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri3"<?= $character->horri3 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri4"<?= $character->horri4 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri5"<?= $character->horri5 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri6"<?= $character->horri6 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri7"<?= $character->horri7 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri8"<?= $character->horri8 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri9"<?= $character->horri9 === null ? : "checked"; ?>>
                            <input type="checkbox" name="horri10"<?= $character->horri10 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="terri">
                        <legend>Niveau 4 (Terrifié)(-5)</legend>
                            <input type="checkbox" name="terri1"<?= $character->terri1 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri2"<?= $character->terri2 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri3"<?= $character->terri3 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri4"<?= $character->terri4 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri5"<?= $character->terri5 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri6"<?= $character->terri6 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri7"<?= $character->terri7 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri8"<?= $character->terri8 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri9"<?= $character->terri9 === null ? : "checked"; ?>>
                            <input type="checkbox" name="terri10"<?= $character->terri10 === null ? : "checked"; ?>>
                    </fieldset>
                    <fieldset name="petrif">
                        <legend>Niveau 5 (Pétrifié)(-7)</legend>
                            <input type="checkbox" name="petrif1"<?= $character->petrif1 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif2"<?= $character->petrif2 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif3"<?= $character->petrif3 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif4"<?= $character->petrif4 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif5"<?= $character->petrif5 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif6"<?= $character->petrif6 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif7"<?= $character->petrif7 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif8"<?= $character->petrif8 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif9"<?= $character->petrif9 === null ? : "checked"; ?>>
                            <input type="checkbox" name="petrif10"<?= $character->petrif10 === null ? : "checked"; ?>>
                    </fieldset>
                </fieldset>
            </div>
            <div id="character-spells" class="tabcontent">
                <fieldset id="spells" form="character-sheet">
                    <p>Foi/Maîtrise du vitner</p>
                    <fieldset id="compmagie-form">
                        <label for="vdc"><input type="number" name="vdc" value='<?= $character === null ? "" : $character->vdc; ?>'> val. de compétence</label>
                        <label for="galda"><input type="number" name="galda" value='<?= $character === null ? "" : $character->galda; ?>'> Galda</label>
                        <label for="sejda"><input type="number" name="sejda" value='<?= $character === null ? "" : $character->sejda; ?>'> Sejda</label>
                        <label for="vyrda"><input type="number" name="vyrda" value='<?= $character === null ? "" : $character->vyrda; ?>'> Vyrda</label>
                    </fieldset>
                    <fieldset id="mana-form">
                        <label for="pdvd"><input type="number" name="pdvd" value='<?= $character === null ? "" : $character->pdvd; ?>'> points de vitner/divinité</label>
                        <label for="ptemp"><input type="number" name="ptemp" value='<?= $character === null ? "" : $character->ptemp; ?>'> points temp.</label>
                    </fieldset>
                    <fieldset id="tablettes-form">
                        <legend>Tablettes</legend>
                        <textarea rows="15" cols="70" name="tablettes"><?= $character === null ? "" : $character->tablettes; ?></textarea>
                    </fieldset>
                </fieldset>
            </div>
        </form>
	</body>
</html>
